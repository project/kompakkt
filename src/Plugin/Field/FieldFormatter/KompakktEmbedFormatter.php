<?php

namespace Drupal\kompakkt\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'kompakkt_embed_field' formatter.
 *
 * @FieldFormatter(
 *   id = "kompakkt_embed_format",
 *   label = @Translation("Iframe"),
 *   module = "kompakkt",
 *   field_types = {
 *     "kompakkt_embed_field"
 *   }
 * )
 */
class KompakktEmbedFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Displays the 3D Model from Kompakkt.');

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      // Render each element as markup.
      $element[$delta] = [
        '#type' => 'inline_template',
        '#template' => '<div class="kompakkt-embed-wrapper"><iframe width="640" height="480" src="{{ url }}/embed" frameborder="0" allowvr allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" onmousewheel=""></iframe>',
        '#context' => [
          'url' => $item->value,
        ],
      ];
    }

    return $element;
  }

}
