<?php

namespace Drupal\kompakkt\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'kompakkt_upload_field' formatter.
 *
 * @FieldFormatter(
 *   id = "kompakkt_upload_format",
 *   label = @Translation("IFrame"),
 *   module = "kompakkt",
 *   field_types = {
 *     "kompakkt_upload_field"
 *   }
 * )
 */
class KompakktUploadFormatter extends FormatterBase {
  /**
   * The current user.
   *
   * @var AccountProxy
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountProxyInterface $currentUser) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Displays the 3D Model from Kompakkt.');

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    $has_permission = $this->currentUser->hasPermission('Kompakkt Write Anno');

    foreach ($items as $delta => $item) {
      $url = "";

      if (isset($item->value)) {
        $url = $item->value;
      }
      else {
        if ($has_permission) {
          $url = "/libraries/kompakkt_viewer/dist/Kompakkt/index.html?entity=" . $item->target_id . "&mode=annotation";
        }
        else {
          $url = "/libraries/kompakkt_viewer/dist/Kompakkt/index.html?entity=" . $item->target_id . "&mode=open/embed";
        }
      }

      // Render each element as markup.
      $element[$delta] = [
        '#type' => 'inline_template',
        '#template' => '<div class="kompakkt-embed-wrapper"><iframe width="640" height="480" src="{{ url }}" frameborder="0" allowvr allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" onmousewheel=""></iframe>',
        '#context' => [
          'url' => $url,
        ],
      ];
    }

    return $element;
  }

}
