<?php

namespace Drupal\kompakkt\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'kompakkt_embed_field' field type.
 *
 * @FieldType(
 *   id = "kompakkt_embed_field",
 *   label = @Translation("Embed Kompakkt"),
 *   description = @Translation("Embed 3D model in pages."),
 *   category = @Translation("Media"),
 *   default_widget = "kompakkt_embed_widget",
 *   default_formatter = "kompakkt_embed_format"
 * )
 */
class KompakktEmbedItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'text',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Target URL'));

    return $properties;
  }

}
