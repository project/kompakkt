<?php

namespace Drupal\kompakkt\Plugin\Field\FieldType;

use Drupal\file\Plugin\Field\FieldType\FileItem;

/**
 * Plugin implementation of the 'kompakkt_upload_field' field type.
 *
 * @FieldType(
 *   id = "kompakkt_upload_field",
 *   label = @Translation("Kompakkt File Upload"),
 *   description = @Translation("Implements a file upload field with Kompakkt"),
 *   category = @Translation("Media"),
 *   default_widget = "kompakkt_upload_widget",
 *   default_formatter = "kompakkt_upload_format"
 * )
 */
class KompakktUploadItem extends FileItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $default = parent::defaultFieldSettings();

    $default['file_extensions'] = 'glb gltf';

    return $default;
  }

}
