<?php

namespace Drupal\kompakkt\Plugin\Field\FieldWidget;

use Drupal\file\Plugin\Field\FieldWidget\FileWidget;

/**
 * Plugin implementation of the 'kompakkt_upload_field' widget.
 *
 * @FieldWidget(
 *   id = "kompakkt_upload_widget",
 *   label = @Translation("Upload 3D file"),
 *   module = "kompakkt",
 *   field_types = {
 *     "kompakkt_upload_field"
 *   }
 * )
 */
class KompakktUploadWidget extends FileWidget {

}
