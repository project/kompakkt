<?php

namespace Drupal\kompakkt\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'kompakkt_embed_field' widget.
 *
 * @FieldWidget(
 *   id = "kompakkt_embed_widget",
 *   label = @Translation("URL of the target"),
 *   module = "kompakkt",
 *   field_types = {
 *     "kompakkt_embed_field"
 *   }
 * )
 */
class KompakktEmbedWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = isset($items[$delta]->value) ? $items[$delta]->value : '';
    $element += [
      '#type' => 'url',
      '#default_value' => $value,
      '#element_validate' => [
        [$this, 'validate'],
      ],
    ];
    return ['value' => $element];
  }

  /**
   * Validate the color text field.
   */
  public function validate($element, FormStateInterface $form_state) {
    $value = $element['#value'];
    if (strlen($value) == 0) {
      $form_state->setValueForElement($element, '');
      return;
    }

  }

}
