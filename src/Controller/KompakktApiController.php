<?php

namespace Drupal\kompakkt\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

use Drupal\file\Entity\File;

/**
 * This class handles annotation on 3D entities in the kompakkt viewer.
 *
 * @package Drupal\Kompakkt\Controller
 */
class KompakktApiController {

  /**
   * The used entity type. This should be made dynamic in future.
   *
   * @var entityType
   */
  public $entityType = "wisski_individual";

  /**
   * The bundle in this entity type. This should be made dynamic in future.
   *
   * @var bundle
   */
  public $bundle = "bae4d4ce498e2d471402a811abebd0ca";

  /**
   * The field pointing to the entity. This should be made dynamic in future.
   *
   * @var fieldToEntity
   */
  public $fieldToEntity = "fdf7d56907c96ea409cae51b7e2b0dd7";

  /**
   * The field storing the json. This should be made dynamic in future.
   *
   * @var fieldToJson
   */
  public $fieldToJson = "fefa8bd45d55f4e193b9c13a89631e35";

  /**
   * The field storing the id. This should be made dynamic in future.
   *
   * @var fieldToId
   */
  public $fieldToId = "f227df64abe3b4ed4ade4b4e20e29cfd";

  /**
   * The main function that gets the json for the entity.
   *
   * @Param $entity_id the id of a kompakkt 3d model - usually a file id!
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The requested entity
   */
  public function getEntity($entity_id) {
    // We assume that the current user wants to display this
    // and probably is allowed to do so because we protect the model
    // with permissions on the drupal layer, not on the kompakkt-layer.
    $curr = \Drupal::currentUser();

    // Get the users id.
    $uid = $curr->id();

    // Get the username.
    $username = $curr->getAccountName();

    // Transform the file_id to file because we need the file-url as
    // this is typically what is stored in WissKI.
    $file = File::load($entity_id);

    if (!$file->access('view')) {
      return new JsonResponse([]);
    }

    $uri = $file->getFileUri();
    $stream_wrapper_manager = \Drupal::service('stream_wrapper_manager')->getViaUri($uri);
    // This is /srv/www/htdocs...
    // $file_path = $stream_wrapper_manager->realpath();
    $file_path = $stream_wrapper_manager->getExternalUrl();

    // Get extension and filename.
    $pathinfo = pathinfo($stream_wrapper_manager->realpath());

    $extension = $pathinfo["extension"];

    $filename = $pathinfo["filename"];

    // Get the mimetype.
    $mimetype = $file->getMimeType();

    $mediatype = explode("/", $mimetype, 2);

    $mediatype = $mediatype[0];

    // If it is application/octet-stream then it is a 3d model.
    if ($mediatype == "application") {
      $mediatype = "model";
    }

    // Fetch the annotations for this file
    // we assume that it is not used multiple times in several datasets!
    $annotation_ids = \Drupal::entityQuery($this->entityType)
      ->condition('bundle', [$this->bundle => $this->bundle])
      ->condition($this->fieldToEntity, $file_path)->execute();

    $annotations = [];

    // More easy mapping for better readable access.
    $fieldToId = $this->fieldToId;
    $fieldToJson = $this->fieldToJson;

    // Iterate the annotations.
    foreach ($annotation_ids as $annotation_id) {
      $one_annotation = \Drupal::service('entity_type.manager')->getStorage($this->entityType)->load($annotation_id);

      $one_annotation = $one_annotation->getValues(TRUE);

      // I dont know why we have to do that here
      //  TODO handle translation somehow!
      $one_annotation = $one_annotation[0];

      // Get the id field of the annotation and the json code of the annotation
      // this could be more elaborate here, e.g. we could extract the
      // annotations contents to a full semantic model etc.
      $main_property_for_fieldToId = $one_annotation[$fieldToId]["main_property"];
      $main_property_for_json = $one_annotation[$fieldToJson]["main_property"];

      // Fetch the contents.
      $my_fieldToId = $one_annotation[$fieldToId][0][$main_property_for_fieldToId];
      $my_json = $one_annotation[$fieldToJson][0][$main_property_for_json];

      // And write it accordingly to the array.
      $annotations[$my_fieldToId] = json_decode($my_json, TRUE);
    }

    // Build the response.
    $response = [
      "_id" => $entity_id,
      "name" => "$filename",
      "dataSource" => [
        "isExternal" => FALSE,
        "service" => "kompakkt",
      ],
      "files" => [[
        "file_name" => "$filename",
        "file_link" => "$file_path",
        "file_size" => 0,
        "file_format" => "." . $extension,
      ],
      ],
      "annotations" => $annotations,
      "relatedDigitalEntity" => ["_id" => "default_entity"],
      "creator" => [
        "_id" => $uid,
        "username" => $username,
      ],
      "finished" => FALSE,
      "online" => FALSE,
      "whitelist" => [
        "enabled" => TRUE,
        "persons" => [["_id" => $uid]],
        "groups" => [],
      ],
      "mediaType" => "$mediatype",
      "processed" => [
        "low" => "$file_path",
        "medium" => "$file_path",
        "high" => "$file_path",
        "raw" => "$file_path",
      ],
      "settings" => [
        "preview" => "previews/entity/5d6f708c72b3dc766b27d750.png",
        "cameraPositionInitial" => [
          "position" => ["x" => 150, "y" => 150, "z" => 150],
          "target" => ["x" => 0, "y" => 0, "z" => 0],
        ],
        "background" => [
          "color" => ["r" => 221, "g" => 128, "b" => 40, "a" => 1],
          "effect" => TRUE,
        ],
        "lights" => [],
        "rotation" => [],
        "scale" => 1,

        "status" => "ok",
      ],
    ];

    return new JsonResponse($response);
  }

  /**
   * The function for the storage of the annotations.
   */
  public function pushAnno() {

    // Somehow kompakkt iterates this several times
    // so we have to make sure that one is written after the other
    // Thanks to Myriel for finding this here. ;-)
    $lock = \Drupal::lock();

    if ($lock->acquire('pushAnno')) {

      $cont = \Drupal::request()->getContent();
      // \Drupal::logger('WissKIsaveProcess')
      // ->debug(__METHOD__ . " with values: " . serialize($cont));
      // Decode the contents
      $contarr = json_decode($cont, TRUE);

      // Could not decode json? Return...
      if (empty($contarr)) {
        return new JsonResponse();
      }

      // Get the file_id that is referenced.
      $file_id = $contarr['target']['source']['relatedEntity'];

      // Get the annotation id.
      $annotation_id = $contarr['_id'];

      // Load the file because we need the uri.
      $file = File::load($file_id);

      $uri = $file->getFileUri();
      $stream_wrapper_manager = \Drupal::service('stream_wrapper_manager')->getViaUri($uri);
      $file_path = $stream_wrapper_manager->getExternalUrl();

      // See if we already have this annotation
      // if not we create it anew
      // if yes, it is an update!
      $entity_ids = \Drupal::entityQuery($this->entityType)
        ->condition('bundle', [$this->bundle => $this->bundle])
        ->condition($this->fieldToId, $annotation_id)->execute();

      // Just take the first, there should not be more than this.
      $entity_id = current($entity_ids);

      if (empty($entity_id)) {

        // Build the vales and create the entity.
        $values = [
          "bundle" => $this->bundle,
          $this->fieldToEntity => $file_path,
          $this->fieldToId => $annotation_id,
          $this->fieldToJson => $cont,
        ];

        $entity = \Drupal::entityTypeManager()->getStorage($this->entityType)->create($values);
      }
      else {
        // Load the entity and change the json.
        $entity = \Drupal::service('entity_type.manager')->getStorage($this->entityType)->load($entity_id);

        $fieldToJson = $this->fieldToJson;

        $entity->$fieldToJson->value = $cont;
      }

      // Finally do a save.
      $entity->save();

      // And release the lock.
      $lock->release("pushAnno");
    }

    return new JsonResponse();
  }

  /***
   * Function to get the user authentication credentials.
   */
  public function getAuth() {

    // Where do we come from?
    // we need that because we don't really know
    // which 3d models the user has created
    // but the user probably wants access to the one
    // where the user comes from, so
    // we grant this access anyway because drupal does the
    // user permission handling.
    $call_url = \Drupal::request()->headers->get('referer');

    preg_match('/\?entity=(.*)\&/', $call_url, $call_eid);

    $call_eid = $call_eid[1];

    // Get the current user.
    $curr = \Drupal::currentUser();

    // Get the userid.
    $uid = $curr->id();

    // The username.
    $username = $curr->getAccountName();

    // And the email address.
    $mail = $curr->getEmail();

    // And write this accordingly.
    $response = [
      "_id" => $uid,
      "username" => $username,
      "prename" => "",
      "surname" => "",
      "mail" => $mail,
      "fullname" => "",
      "role" => "uploader",
      "userOwnsEntity" => TRUE,
      "data" => [
        "address" => [],
        "annotation" => [],
        "compilation" => [],
        "contact" => [],
        "digitalentity" => [],
        "entity" => [[
          "_id" => $call_eid,
        ],
        ],
      ],
    ];

    return new JsonResponse($response);

  }

}
