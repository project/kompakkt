CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
This module embeds the Kompakkt 3D Viewer (https://kompakkt.de/) into your
Drupal 8/9 system. 

Short tutorial:
1. Create/Edit a content type
2. Add a new field of type 'Embed Kompakkt' and configure it to use the "URL
of the target" widget for input
3. Create/Edit a page of the content type
4. Paste the URL of a 3D model link (e.g.
https://kompakkt.de/viewer/index.html?entity=5d6f708c72b3dc766b27d750&mode=embed)
into the given field and press save
5. See the 3D model in your dataset


REQUIREMENTS
------------
This module requires no modules outside of Drupal core. If you want to use
annotations with local storage in your system you need to install the
Kompakkt Viewer library (see Installation).

INSTALLATION
------------
Install the Kompakkt module as you would normally install a contributed 
Drupal module.

If you want to use Kompakkt with locally stored annotation you need to check 
out the Kompakkt Viewer library in your libraries directory:
git checkout https://github.com/Kompakkt/Viewer.git kompakkt_viewer

Afterwards you need to follow the building documentation at
https://github.com/Kompakkt/Viewer:
$ git clone https://github.com/Kompakkt/Common.git src/common
$ npm i

and
$ ng build

CONFIGURATION
-------------
After enabling the module you can select the new field type "Embed
Kompakkt" when creating a new field. This field has two form widgets, you
can either:
Store your 3D models on the official Kompakkt.de-server or on an own server
instance maintained by you. Therefore you can embed links to these servers
via the "URL of the target" widget. On data input you can directly use the
embed link of Kompakkt e.g. https://kompakkt.de/viewer/index.html?entity=5d6f708c72b3dc766b27d750&mode=embed
should embed nicely.

Or:
You can upload your 3D models to your Drupal. Therefore you need to select
the "Upload 3D file" widget and configure it to accept the file formats that
Kompakkt can proecess. Currently this is mainly the GLTF format, but some
other (one file based) formats are working, too.

Local annotation storage is currently in development. The documentation will
follow as soon as it reached a stable state.
 
MAINTAINERS
----------

Thanks to https://www.drupal.org/project/sketchfab esp. thedragonslayer
(https://www.drupal.org/u/thedragonslayer). I've built this Module based on
his work. 

**How to Use**
1. Create/Edit a Content type
2. Add Field 'Embeded Kompakkt'
3. Create/Edit a page of the Content type
4. Paste the URL into the given field
